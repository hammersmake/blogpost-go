package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"reflect"
	"strings"

	"github.com/bmuller/arrow"
	"github.com/google/generative-ai-go/genai"
	"github.com/joho/godotenv"
	"github.com/metal3d/go-slugify"
	"google.golang.org/api/option"
)

type FrontMatter struct {
	Date  string   `json:"date"`
	Slug  string   `json:"slug"`
	Tags  []string `json:"tags"`
	Title string   `json:"title"`
}

func CreateGallery(blogpath string) {
	//Create a folder/directory at a full qualified path
	thepath := blogpath + "/content/galleries/" + arrow.Now().CFormat("%Y/%m")

	fmt.Println(thepath)
	err := os.MkdirAll(thepath, 0755)
	if err != nil {
		log.Fatal(err)
	}
}

func CopyGallery(blogpath string, srcdirc string) {
	files, err := ioutil.ReadDir(srcdirc)
	if err != nil {
		log.Fatal(err)
	}

	for _, file := range files {
		src := srcdirc + "/" + file.Name()
		//dst := desdir

		fin, err := os.Open(src)
		if err != nil {
			log.Fatal(err)
		}
		defer fin.Close()
		thepath := blogpath + "/content/galleries/" + arrow.Now().CFormat("%Y/%m")
		fout, err := os.Create(thepath + "/" + file.Name())
		if err != nil {
			log.Fatal(err)
		}
		defer fout.Close()

		_, err = io.Copy(fout, fin)

		if err != nil {
			log.Fatal(err)
		}

	}
	//(originpath string, blogpath string) {

}
func CreateContent(blogpath string, postitle string) {
	ctx := context.Background()

	err := godotenv.Load(".env")
	if err != nil {
		log.Fatalf("Some error occured. Err: %s", err)
	}

	client, err := genai.NewClient(ctx, option.WithAPIKey(os.Getenv("GENAPI")))
	if err != nil {
		log.Fatal(err)
	}
	defer client.Close()
	vmodel := client.GenerativeModel("gemini-pro-vision")
	slug := slugify.Marshal(postitle)
	thepath := blogpath + "/content/galleries/" + arrow.Now().CFormat("%Y/%m")
	files, err := os.ReadDir(thepath)
	if err != nil {
		log.Fatal(err)
	}

	for _, file := range files {
		fmt.Println(file.Name(), file.IsDir())
		filname := file.Name()
		na := strings.Replace(filname, ".png", "", -1)
		mdpath := blogpath + "/content/posts/" + slug + ".md"
		f, err := os.OpenFile(mdpath,
			os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
		if err != nil {
			log.Println(err)
		}

		data, err := os.ReadFile(filepath.Join(thepath+"/", file.Name()))
		if err != nil {
			log.Fatal(err)
		}
		resp, err := vmodel.GenerateContent(ctx, genai.Text("Write a 3rd person response to this artwork for a blog post that describes the content and drawing techniques."), genai.ImageData("png", data))
		fmt.Printf("resp: %v\n", resp)
		if err != nil {
			log.Fatal(err)
		}

		defer f.Close()

		if _, err := f.WriteString("\n![" + na + "]" + "(/galleries/" + arrow.Now().CFormat("%Y/%m/") + filname + ")\n\n" + fmt.Sprintf("%v", resp.Candidates[0].Content.Parts[0]) + "\n"); err != nil {
			log.Println(err)
		}
	}
}

func CreatePost(blogpath string, postitle string, postags string) {
	slug := slugify.Marshal(postitle)

	splittag := strings.Split(postags, ",")
	fmt.Println(reflect.TypeOf(splittag))
	newpost := &FrontMatter{
		Date:  arrow.Now().CFormat("%Y-%m-%d"),
		Slug:  slug,
		Tags:  splittag,
		Title: postitle,
	}
	b, err := json.Marshal(newpost)
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println(string(b))
	f, err := os.Create(blogpath + "/content/posts/" + slug + ".md")
	if err != nil {
		fmt.Println(err)
		return
	}
	l, err := f.WriteString(string(b))
	if err != nil {
		fmt.Println(err)
		f.Close()
		return
	}
	fmt.Println(l, "bytes written successfully")
	err = f.Close()
	if err != nil {
		fmt.Println(err)
		return
	}
}
