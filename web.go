package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

type PostInput struct {
	BlogPath string `json:"blogpath" binding:"required"`
	SrcPath  string `json:"srcpath" binding:"required"`
	Title    string `json:"title" binding:"required"`
	Tags     string `json:"tags" binding:"required"`
}

func main() {
	r := gin.Default()
	r.POST("/createpost", func(c *gin.Context) {
		var input PostInput

		if err := c.ShouldBindJSON(&input); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}
		CreateGallery(input.BlogPath)
		CopyGallery(input.BlogPath, input.SrcPath)
		CreatePost(input.BlogPath, input.Title, input.Tags)
		CreateContent(input.BlogPath, input.Title)
		c.JSON(http.StatusOK, gin.H{"success": "True"})
	})
	r.Run()
}
