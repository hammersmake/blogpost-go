package main

import (
	"io/ioutil"
	"log"
	"os"
	"testing"

	"github.com/bmuller/arrow"
)

func TestCreateGallery(t *testing.T) {
	CreateGallery("/home/pi/artctrl")
	thepath := "/home/pi/artctrl/galleries/" + arrow.Now().CFormat("%Y/%m/%d")
	_, err := os.Stat(thepath)
	if os.IsNotExist(err) {
		t.Error("Folder does not exist.")
	}
	erring := os.Remove(thepath)
	if erring != nil {
		log.Fatal(erring)
	}
}
func TestCopyGallery(t *testing.T) {
	CopyGallery("/home/pi/artctrl", "/home/pi/june21")
	thepath := "/home/pi/artctrl/galleries/" + arrow.Now().CFormat("%Y/%m/%d")
	files, err := ioutil.ReadDir(thepath)
	if err != nil {
		log.Fatal(err)
	}
	expected := "phillgchq-Sister.png"
	t.Error("expected %S but got %S", expected, files)
}
